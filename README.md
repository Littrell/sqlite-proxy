# sqlite-proxy

sqlite-proxy is a simple HTTP service that proxies queries through to SQLite and returns the results in JSON.

## Build

```bash
go build -o sqlite-proxy *.go
```

## Run

```bash
./sqlite-proxy
```

## Example query

```bash
curl \
  --location --request POST 'localhost:8080/query' \
  --header 'Content-Type: application/json' \
  --data-raw '{
    "query": "SELECT * FROM test",
    "path": "./test.db"
  }'
```

