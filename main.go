package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

type QueryRequest struct {
	Query string `json:"query"`
	Path  string `json:"path"`
}

type CellInfo struct {
	ColumnIndex int         `json:"columnIndex"`
	ColumnName  string      `json:"columnName"`
	Value       interface{} `json:"value"`
}

type ColumnInfo struct {
	Cid       int
	Name      string
	Type      string
	NotNull   int
	DfltValue string
	Pk        int
}

func main() {
	port := os.Getenv("SQLITE_PROXY_PORT")
	if port == "" {
		port = "8080"
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
		return
	})

	http.HandleFunc("/query", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		// Check for POST request
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Must use POST"))
			return
		}

		// Decode the POST body
		decoder := json.NewDecoder(r.Body)
		var queryRequest QueryRequest
		err := decoder.Decode(&queryRequest)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("Invalid request: %v", err)))
			return
		}

		// Check that path isn't empty
		if queryRequest.Path == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Must provide 'path' in POST body"))
			return
		}

		// Check that query isn't empty
		if queryRequest.Query == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Must provide 'query' in POST body"))
			return
		}

		db, err := sql.Open("sqlite3", queryRequest.Path)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("There was an issue opening the database: %v", err)))
			return
		}
		defer db.Close()

		data, err := queryToJson(db, queryRequest.Query)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("There was an issue opening the database: %v", err)))
			return
		}

		json.NewEncoder(w).Encode(data)
	})

	log.Println(fmt.Sprintf("Listening on localhost:%v", port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), nil))
}

func queryToJson(db *sql.DB, query string, args ...interface{}) ([][]CellInfo, error) {
	var rows [][]CellInfo

	results, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	for results.Next() {
		columns, err := results.ColumnTypes()
		if err != nil {
			return nil, err
		}

		values := make([]interface{}, len(columns))
		cells := []CellInfo{}
		for i, column := range columns {
			var v interface{}

			switch column.DatabaseTypeName() {
			case "text":
				v = new(string)
			default:
				v = new(interface{})
			}

			cell := CellInfo{
				ColumnIndex: i,
				ColumnName:  column.Name(),
				Value:       v,
			}
			cells = append(cells, cell)
			values[i] = v
		}

		err = results.Scan(values...)
		if err != nil {
			return nil, err
		}

		rows = append(rows, cells)
	}

	return rows, nil
}
